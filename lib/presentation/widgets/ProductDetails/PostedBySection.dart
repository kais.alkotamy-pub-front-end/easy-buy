import 'package:flutter/material.dart';

class PostedBySection extends StatefulWidget {
  @override
  _PostedBySectionState createState() => _PostedBySectionState();
}

class _PostedBySectionState extends State<PostedBySection> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      color: Colors.white,
      child: Column(
        children: [
          Row(
            children: [
              Text(
                'Posted by:',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              )
            ],
          ),
          SizedBox(
            height: 8,
          ),
          Row(mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                padding: EdgeInsets.all(32),
                child: Icon(
                  Icons.share,
                  color: Colors.orange,
                ),
                decoration:
                    BoxDecoration(color: Colors.black, shape: BoxShape.circle),
              ),
              Container(
                alignment: Alignment.center,
                child: Column(
                children: [
                  Text('Soso',style: TextStyle(fontWeight: FontWeight.bold),),
                  Text('Member Since Jan 12/2021',style: TextStyle(fontSize: 12, color: Colors.grey), softWrap: true,),
                ],
              ),)
            ],
          ),
        ],
      ),
    );
  }
}

import 'package:easybuy/data/models/categoryModel.dart';
import 'package:easybuy/presentation/page/mixins/mainPageMixin.dart';
import 'package:easybuy/presentation/widgets/catSlider.dart';
import 'package:easybuy/presentation/widgets/catgrid.dart';
import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  List<CatModel> catList = [];

  MainPage(this.catList);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with MainPageMixin {
  // List<CatModel> catList = [];
  // _MainPageState(this.catList);
  bool subCat;
  @override
  void initState() {
    subCat =
        widget.catList[0].photoUrl == 'assets/icons/car.png' ? false : true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Visibility(
            visible: subCat ? true : false,
            child: CatSlider(),
          ),
          Expanded(
            child: CustomScrollView(
              slivers: [
                CatGrid(catList: widget.catList,source: 'mainPage',),
                // getCategoryGrid(context, catList),
                getSlidersList(context),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
